package edu.nso.springmongodb.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Label {
    @Id
    private String id;

    private String name;
}

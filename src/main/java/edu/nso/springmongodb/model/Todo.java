package edu.nso.springmongodb.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
public class Todo {
    @Id
    private String id;

    private String name;

    private List<Label> labels;
}

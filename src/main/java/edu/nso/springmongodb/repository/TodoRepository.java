package edu.nso.springmongodb.repository;

import edu.nso.springmongodb.model.Todo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "/todo")
public interface TodoRepository extends MongoRepository<Todo, String> {
}

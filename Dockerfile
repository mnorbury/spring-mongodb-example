FROM java:8-alpine

COPY ./target/spring-mongodb.jar /code/

CMD ["java", "-jar", "/code/spring-mongodb.jar"]